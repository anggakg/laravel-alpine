FROM alpine:3.9

WORKDIR /var/www/html/

# Essentials
RUN apk add --no-cache tzdata
ENV TZ=Asia/Jakarta

RUN apk add --no-cache zip unzip curl nginx supervisor

# Installing bash
RUN apk add bash curl wget git
RUN sed -i 's/bin\/ash/bin\/bash/g' /etc/passwd

# Installing PHP
RUN apk add --no-cache php7 \
    php7-common \
    php7-fpm \
    php7-pdo \
    php7-opcache \
    php7-zip \
    php7-gd \
    php7-phar \
    php7-iconv \
    php7-cli \
    php7-curl \
    php7-openssl \
    php7-mbstring \
    php7-exif \
    php7-tokenizer \
    php7-fileinfo \
    php7-json \
    php7-xml \
    php7-xmlreader \
    php7-xmlwriter \
    php7-simplexml \
    php7-dom \
    php7-pdo_mysql \
    php7-tokenizer \
    php7-pecl-redis

RUN ln -sf /usr/bin/php7 /usr/bin/php

# Installing composer
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN rm -rf composer-setup.php

#RUN apk add nodejs yarn

# Configure supervisor
RUN mkdir -p /etc/supervisor.d/
COPY ./docker/supervisord.ini /etc/supervisor.d/supervisord.ini

# Configure PHP
RUN mkdir -p /run/php/
RUN touch /run/php/php7-fpm.pid

COPY ./docker/php-fpm.conf /etc/php7/php-fpm.conf
COPY ./docker/php.ini-production /etc/php7/php.ini

# Configure nginx
COPY ./docker/nginx.conf /etc/nginx/
COPY ./docker/nginx-laravel.conf /etc/nginx/http.d/default.conf

RUN mkdir -p /run/nginx/
RUN touch /run/nginx/nginx.pid

RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

# Building process
COPY . .
RUN composer install
RUN chown -R nobody:nobody /var/www/html/storage

# add log for supervisor laravel worker
RUN touch /var/www/html/storage/logs/worker.log

# Generate Laravel app encryption key
RUN cp .env.example .env
RUN php artisan key:generate --ansi
RUN php artisan vendor:publish --all

RUN chown -R nginx:nginx /var/www/html -v
RUN chmod -R 777 /var/www/html -v
RUN chown -R nginx:nginx /var/lib/nginx -v
RUN chmod -R 755 /var/lib/nginx -v
RUN chmod -R 755 /var/log/nginx -v

# Exposing port 80 (http)
EXPOSE 5757

# Auto start supervisor on start
CMD ["/usr/bin/supervisord"]
